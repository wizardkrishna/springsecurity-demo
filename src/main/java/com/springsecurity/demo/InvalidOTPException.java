package com.springsecurity.demo;

import org.springframework.security.core.AuthenticationException;

public class InvalidOTPException extends AuthenticationException {
    public InvalidOTPException() {
        super("Invalid OTP");
    }
}
