package com.springsecurity.demo;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Controller
public class MfaController {

    private final AuthenticationSuccessHandler successHandler;

    private final AuthenticationFailureHandler failureHandler;

    private final OTPService otpService;

    private final SMSService smsService;

    public MfaController(OTPService otpService, SMSService smsService, AuthenticationSuccessHandler successHandler,
                         AuthenticationFailureHandler failureHandler) {
        this.otpService = otpService;
        this.successHandler = successHandler;
        this.failureHandler = failureHandler;
        this.smsService = smsService;
    }

    @GetMapping("/otppage")
    public String generateOTP(Model model) throws Exception {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        int otp = otpService.generateOTP(username);
        //Generate The Template to send OTP
        Map<String, String> replacements = new HashMap<String, String>();
        replacements.put("user", username);
        replacements.put("otpnum", String.valueOf(otp));
        String message = "Your OTP is {{otpnum}}";

        model.addAttribute("otpmessage", "OTP " + otp);

        smsService.sendOtpMessage("<mobile number>", "OTP -SpringBoot", message);

        return "otppage";
    }

    @RequestMapping(value = "/validateOtp", method = RequestMethod.POST)
    public void validateOtp(@RequestParam("otpnum") int otpnum, MfaAuthentication authentication,
                       HttpServletRequest request, HttpServletResponse response) throws Exception {
        final String SUCCESS = "Entered Otp is valid";
        final String FAIL = "Entered Otp is NOT valid. Please Retry!";
        String username = authentication.getName();

        //Validate the Otp
        int serverOtp = otpService.getOtp(username);
        if (otpnum == serverOtp) {
            otpService.clearOTP(username);
            SecurityContextHolder.getContext().setAuthentication(authentication.getFirst());
            successHandler.onAuthenticationSuccess(request, response, authentication.getFirst());
        } else {
            failureHandler.onAuthenticationFailure(request, response, new InvalidOTPException());
        }
    }
}